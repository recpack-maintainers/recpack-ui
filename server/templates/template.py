import os
import pandas as pd

IMPORTS
import recpack.pipelines

if not os.path.exists('data'):
    os.mkdir('data')

CREATE_DATASET
dataset.fetch_dataset()

PREPROCESSING_STEPS

data = dataset.load()

CREATE_SCENARIO
scenario.split(data)

pipeline_builder = recpack.pipelines.PipelineBuilder('demo')
pipeline_builder.set_data_from_scenario(scenario)

POSTPROCESSING_STEPS

# add algorithms and metrics here

# pipeline_builder.add_algorithm('ItemKNN', params={'K': 200})
# pipeline_builder.set_optimisation_metric('RecallK', 10)
# pipeline_builder.add_metric('CoverageK', 10)

pipeline = pipeline_builder.build()
pipeline.run()

def output():
    import platform
    import time
    import json
    import datetime
    from pprint import pprint

    # data filled in by template
    # should be generated from pipeline
    # ids should maybe be classnames instead
    EXPERIMENT_SETTING_DICT

    # enviornment info
    import recpack
    import numpy
    import scipy
    import pandas
    import torch
    environment = {
        "os": platform.platform(terse=True),
        "python": platform.python_version(),
        "recpack": getattr(recpack, '__version__', None),
        "dep": {
        "numpy": getattr(numpy, '__version__', None),
        "scipy": getattr(scipy, '__version__', None),
        "pandas": getattr(pandas, '__version__', None),
        "torch": getattr(torch, '__version__', None)
        }
    }

    # for each algorithm, create an experiment result file with metrics
    metrics_df = pipeline.get_metrics()
    for index, row in metrics_df.iterrows():
        # get algorithm and hyperparameters
        # note: the string representation is parsed to extract information -> should be handled more safely inside the pipeline
        ad = index.split('(')
        algorithm_name = ad[0]
        hyperparameter = {s.split('=')[0]: s.split('=')[1] for s in ad[1].split(')')[0].split(',')}

        # timing code needs to be added inside the pipeline if these attributes are to be included
        optimization_duration = 182 # time in seconds to find optimized hyperparams
        training_duration = 182 # time in seconds to train using optimized/given hyperparams
        evaluation_duration = 182 # time in seconds to calcualte all metrics

        # should be extracted in pipeline
        model_size = 3500 # final model size in bytes
        
        # format metrics properly
        # note: metrics string is parsed to extract metric id and k -> should be handled more safely inside the pipeline
        metrics = []
        for m in metrics_df:
            metrics.append({
                "metric_id": m.split('_')[0][:-1],
                "metric_k": int(m.split('_')[1]),
                "value": row[m]
            })
        
        result = {
            "experiment_setting": experiment_setting,
            "environment": environment,
            "model_size": model_size,
            "optimization_duration": optimization_duration,
            "training_duration": training_duration,
            "evaluation_duration": evaluation_duration,
            "created_timestamp": time.time(),
            "algorithm_name": algorithm_name, 
            "hyperparameters": hyperparameter,
            "results": metrics
        }

        with open(f'{algorithm_name} {datetime.datetime.now().strftime("%Y-%m-%d at %H:%M:%S")}.json', 'w') as f:
            f.write(json.dumps(result))

output()
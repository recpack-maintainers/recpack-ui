from flask import request, jsonify, make_response
from flask_restful import Resource, abort

from app.models import * 
from app import utils

class ExperimentSettingsResource(Resource):
  def get(self):
    experiment_settings = db.session.query(ExperimentSetting).all()

    result = []
    for e in experiment_settings:
      top_experiment = e.get_top_experiment()

      result.append({
        **ExperimentSettingDisplaySchema().dump(e),
        "top_experiment": {
          "algorithm_name": top_experiment.algorithm.name,
          "algorithm_id": top_experiment.algorithm.id,
          "experiment_id": top_experiment.id,
       } if top_experiment is not None else None,
       "experiment_count": len(e.experiments)
      })
    
    return result

class ExperimentSettingResource(Resource):
  def get(self, experiment_setting_id):
    experiment_setting = db.session.query(ExperimentSetting).get(experiment_setting_id)
    top_experiment = experiment_setting.get_top_experiment()
    
    ranking_info = experiment_setting.get_algorithm_ranking()
    ranking_info = {r.algorithm_id: r.rank for r in ranking_info}
    for e in experiment_setting.experiments:
      if e.algorithm_id not in ranking_info.keys():
        ranking_info[e.algorithm_id] = -1

    return {
      "experiment_setting": {
        **ExperimentSettingDisplaySchema().dump(experiment_setting),
        "experiment_count": len(experiment_setting.experiments),
        "top_experiment": {
          "algorithm_name": top_experiment.algorithm.name,
          "algorithm_id": top_experiment.algorithm.id,
          "experiment_id": top_experiment.id,
       } if top_experiment is not None else None
      },
      "experiments": ExperimentDisplaySchema(many=True).dump(experiment_setting.experiments),
      "ranking_info": ranking_info,
      
    }

class ExperimentSettingTemplateResource(Resource):

  def get(self, experiment_setting_id):
    experiment_setting = db.session.query(ExperimentSetting).get(experiment_setting_id)

    resp = make_response(utils.create_template(experiment_setting), 200)
    # resp.headers={'Content-Type': 'application/json'}
    resp.headers={'Content-Type': 'text/plain', 'Content-Disposition': 'attachment; filename="template.py"'}
    return resp
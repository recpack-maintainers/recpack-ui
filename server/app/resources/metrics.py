from flask import request, jsonify
from flask_restful import Resource, abort

from app.models import * 

SCHEMA = MetricSchema(many=True)

class MetricsResource(Resource):
  def get(self):
    results = db.session.query(Metric).all()
    return SCHEMA.dump(results) 
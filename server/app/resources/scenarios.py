from flask import request, jsonify
from flask_restful import Resource, abort

from app.models import * 

SCHEMA = ScenarioSchema(many=True)

class ScenariosResource(Resource):
  def get(self):
    results = db.session.query(Scenario).all()
    return SCHEMA.dump(results) 
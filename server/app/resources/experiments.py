from flask import request, jsonify
from flask_restful import Resource, abort

from app.models import * 

class ExperimentResource(Resource):
  def get(self, experiment_id):
    experiment = db.session.query(Experiment).get(experiment_id)
    return ExperimentDisplaySchema().dump(experiment) 
from flask import request, jsonify
from flask_restful import Resource, abort

from app.models import * 

class AlgorithmsResource(Resource):
  def get(self):
    algorithms = db.session.query(Algorithm).all()

    result = []
    for a in algorithms:
      result.append({
        **AlgorithmSchema().dump(a),
        "experiment_count": len(a.experiments),
        "experiment_settings_count": len(set([e.experiment_setting_id for e in a.experiments]))
      })
    
    return result

class AlgorithmResource(Resource):
  def get(self, algorithm_id):
    a = db.session.query(Algorithm).get(algorithm_id)

    ranking_info = a.get_rankings()
    
    for e in a.experiments:
      if e.experiment_setting_id not in ranking_info.keys():
        ranking_info[e.experiment_setting_id] = -1

    return {
      **AlgorithmSchema().dump(a),
      "experiments": ExperimentDisplaySchema(many=True).dump(a.experiments),
      "ranking_info": ranking_info
    }
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, post_dump, pre_dump
from app import db

from app.models.metrics import MetricSchema

class Result(db.Model):
  __tablename__ = 'results'

  # attributes
  experiment_id = db.Column(db.ForeignKey('experiments.id', ondelete='CASCADE'), primary_key=True)
  metric_id = db.Column(db.ForeignKey('metrics.id', ondelete='CASCADE'), primary_key=True)
  metric_k = db.Column(db.Integer, primary_key=True)
  value = db.Column(db.Float)

  # relations
  experiment = db.relationship('Experiment', back_populates='results')
  metric = db.relationship('Metric')

class ResultSchema(Schema):
  experiment_id = fields.Int()
  metric_id = fields.Str()
  metric_k = fields.Int()
  value = fields.Float()

  @post_load
  def to_database_record(self, data, **kwargs):
    return Result(**data)
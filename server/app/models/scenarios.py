from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load
from app import db

class Scenario(db.Model):
  __tablename__ = 'scenarios'

  # attributes
  id = db.Column(db.String, primary_key=True)
  name = db.Column(db.String)
  class_name = db.Column(db.String)
  description = db.Column(db.String)
  recpack = db.Column(db.String)

  # relations
  experiment_settings = db.relationship('ExperimentSetting', back_populates='scenario')


class ScenarioSchema(Schema):
  id = fields.Str()
  name = fields.Str()
  class_name = fields.Str()
  description = fields.Str()
  recpack = fields.Url()

  @post_load
  def to_database_record(self, data, **kwargs):
      return Scenario(**data)
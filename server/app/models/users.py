from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, EXCLUDE
from app import db

class User(db.Model):
  __tablename__ = 'users'

  # attributes
  id = db.Column(db.String, primary_key=True)
  username = db.Column(db.String)
  email = db.Column(db.String)

  # relationships
  experiments = db.relationship('Experiment', back_populates='uploader')


class UserSchema(Schema):
  id = fields.Str()
  username = fields.Str()
  email = fields.Email()

  @post_load
  def to_database_record(self, data, **kwargs):
      return User(**data)
from marshmallow import Schema, fields, post_load

class StatSchema(Schema):
  interactions = fields.Int(load_default=0)
  users = fields.Int(load_default=0)
  items = fields.Int(load_default=0)
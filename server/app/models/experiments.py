from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
from marshmallow import Schema, fields, post_load, post_dump, pre_dump, EXCLUDE
from app import db

from app.models.algorithms import AlgorithmSchema
from app.models.experiment_settings import ExperimentSettingDisplaySchema
from app.models.results import ResultSchema
from app.models.metrics import MetricSchema
from app.models.users import UserSchema

class Experiment(db.Model):
  __tablename__ = 'experiments'

  # attributes
  id = db.Column(db.Integer, primary_key=True)
  algorithm_id = db.Column(db.ForeignKey('algorithms.id', ondelete='CASCADE', onupdate='CASCADE'))
  experiment_setting_id = db.Column(db.ForeignKey('experiment_settings.id', ondelete='CASCADE', onupdate='CASCADE'))
  hyperparameters = db.Column(db.PickleType)
  optimization_config = db.Column(db.PickleType)
  environment = db.Column(db.PickleType)
  model_size = db.Column(db.Integer)
  training_duration = db.Column(db.Integer)
  evaluation_duration = db.Column(db.Integer)
  
  verified = db.Column(db.Boolean, default=False)
  created_timestamp = db.Column(db.DateTime, server_default=func.now())
  updated_timestamp = db.Column(db.DateTime, onupdate=func.now())
  uploader_id = db.Column(db.ForeignKey('users.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=True)

  # relations
  algorithm = db.relationship('Algorithm', back_populates='experiments')
  experiment_setting = db.relationship('ExperimentSetting', back_populates='experiments')
  results = db.relationship('Result', back_populates='experiment')
  uploader = db.relationship('User', back_populates='experiments')


class ExperimentDisplaySchema(Schema):
  id = fields.Int()
  algorithm = fields.Nested(AlgorithmSchema)
  experiment_setting = fields.Nested(ExperimentSettingDisplaySchema(only=('id', 'name', 'dataset', 'scenario')))
  results = fields.List(fields.Nested(ResultSchema))
  hyperparameters = fields.Dict()
  optimization_config = fields.Dict()
  environment = fields.Dict()
  model_size = fields.Int()
  training_duration = fields.Int()
  evaluation_duration = fields.Int()
  verified = fields.Bool()
  created_timestamp = fields.Date(format="%d/%m/%Y")
  uploader = fields.Nested(UserSchema())
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, pre_dump
from app import db
import json

from app.models.filters import FilterSchema

class ExperimentSettingFilter(db.Model):
  __tablename__ = 'experiment_settings_filters'

  # attributes
  id = db.Column(db.Integer, primary_key=True)
  order = db.Column(db.Integer)
  experiment_setting_id = db.Column(db.ForeignKey('experiment_settings.id', ondelete='CASCADE', onupdate='CASCADE'))
  filter_id = db.Column(db.ForeignKey('filters.id', ondelete='CASCADE', onupdate='CASCADE'))
  params = db.Column(db.PickleType)
  is_preprocessing = db.Column(db.Boolean)

  # relations
  filter = db.relationship('Filter')
  experiment_setting = db.relationship('ExperimentSetting', back_populates="filters")


class ExperimentSettingFilterImportSchema(Schema):
  order = fields.Int()
  experiment_setting_id = fields.Int()
  filter_id = fields.Str()
  params = fields.Dict()
  is_preprocessing = fields.Bool()

  @post_load
  def to_database_record(self, data, **kwargs):
    return ExperimentSettingFilter(**data)

class ExperimentSettingFilterSchema(Schema):
  filter = fields.Nested(FilterSchema(only=("name", "description", "id")))
  params = fields.Dict()

class ExperimentSettingFilterSchema2(Schema):
  filter_id = fields.Str()
  params = fields.Dict()
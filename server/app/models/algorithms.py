from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, post_dump, pre_dump, EXCLUDE
from sqlalchemy import func
from app import db

from app.models.experiment_settings import ExperimentSetting
from app.models.results import Result


class Algorithm(db.Model):
  __tablename__ = 'algorithms'

  # attributes
  id = db.Column(db.String, primary_key=True)
  name = db.Column(db.String)
  description = db.Column(db.String)

  # relations
  experiments = db.relationship('Experiment', back_populates='algorithm')

  def get_rankings(self, ranking_metric=None, ranking_metric_k=None):
    from app.models.experiments import Experiment
    """
    For each experiment setting, determine the ranking of this algorithm for that experiment setting
    """

    experiment_settings = db.session.query(ExperimentSetting) \
      .join(ExperimentSetting.experiments) \
      .filter(Experiment.algorithm == self) \
      .group_by(ExperimentSetting.id) \
      .all()

    result = dict()
    for experiment_setting in experiment_settings:
      ranking = experiment_setting.get_algorithm_ranking(ranking_metric, ranking_metric_k)

      rank = next((r.rank for r in ranking if r.algorithm_id == self.id), None)
      if rank is not None:
        result[experiment_setting.id] = rank

    return result


class AlgorithmSchema(Schema):
  id = fields.Str()
  name = fields.Str()
  description = fields.Str()

  @post_load
  def to_database_record(self, data, **kwargs):
    return Algorithm(**data)

  class Meta:
    unknown = EXCLUDE

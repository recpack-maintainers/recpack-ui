from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, EXCLUDE
from app import db

class Metric(db.Model):
  __tablename__ = 'metrics'

  # attributes
  id = db.Column(db.String, primary_key=True)
  name = db.Column(db.String)
  minimise = db.Column(db.Boolean)


class MetricSchema(Schema):
  id = fields.Str()
  name = fields.Str()
  minimise = fields.Bool(load_default=False)

  @post_load
  def to_database_record(self, data, **kwargs):
      return Metric(**data)

  class Meta:
    unknown = EXCLUDE
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, post_dump, pre_dump
from app import db

from app.models.stats import StatSchema

class Dataset(db.Model):
  __tablename__ = 'datasets'

  # attributes
  id = db.Column(db.String, primary_key=True)
  name = db.Column(db.String)
  class_name = db.Column(db.String)
  description = db.Column(db.String)
  recpack = db.Column(db.String)

  interaction_count = db.Column(db.Integer, nullable=True)
  user_count = db.Column(db.Integer, nullable=True)
  item_count = db.Column(db.Integer, nullable=True)

  # relations
  experiment_settings = db.relationship('ExperimentSetting', back_populates='dataset')


class DatasetSchema(Schema):
  id = fields.Str()
  name = fields.Str()
  class_name = fields.Str()
  description = fields.Str()
  recpack = fields.Url()
  stats = fields.Nested(StatSchema, load_default=StatSchema().load({}))

  @post_load
  def to_database_record(self, data, **kwargs):
    return Dataset(
      **{k: v for (k, v) in data.items() if k != 'stats'},
      interaction_count=data["stats"]["interactions"],
      user_count=data["stats"]["users"],
      item_count=data["stats"]["items"],
    )

  @pre_dump
  def from_database_record(self, record, **kwargs):
    return {
      "id": record.id,
      "name": record.name,
      "class_name": record.class_name,
      "description": record.description,
      "recpack": record.recpack,
      "stats": {
        "interactions": record.interaction_count,
        "users": record.user_count,
        "items": record.item_count
      }
    }
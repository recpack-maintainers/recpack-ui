from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_load, pre_dump, EXCLUDE
from app import db
import json
from sqlalchemy import func

from app.models.datasets import DatasetSchema
from app.models.scenarios import ScenarioSchema
from app.models.stats import StatSchema
from app.models.metrics import MetricSchema
from app.models.filters import FilterSchema
from app.models.experiment_settings_filters import ExperimentSettingFilterSchema, ExperimentSettingFilterSchema
from app.models.results import Result


class ExperimentSetting(db.Model):
  __tablename__ = 'experiment_settings'

  ### attributes
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String, nullable=False)
  dataset_id = db.Column(db.ForeignKey('datasets.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False)
  scenario_id = db.Column(db.ForeignKey('scenarios.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False)
  scenario_params = db.Column(db.PickleType)
  ranking_metric_id = db.Column(db.ForeignKey('metrics.id'), nullable=True)
  ranking_metric_k = db.Column(db.Integer)

  # preprocessing_stats
  interaction_count = db.Column(db.Integer, nullable=True)
  user_count = db.Column(db.Integer, nullable=True)
  item_count = db.Column(db.Integer, nullable=True)

  ### relations
  dataset = db.relationship('Dataset', back_populates='experiment_settings')
  scenario = db.relationship('Scenario', back_populates='experiment_settings')
  ranking_metric = db.relationship('Metric')
  
  filters = db.relationship('ExperimentSettingFilter', back_populates="experiment_setting")
  experiments = db.relationship('Experiment', back_populates="experiment_setting")

  def get_top_experiment(self, ranking_metric=None, ranking_metric_k=None):
    """
    Determine the top experiment for a given experiment_setting
    """

    metric = self.ranking_metric if ranking_metric is None else ranking_metric
    metric_k = self.ranking_metric_k if ranking_metric_k is None else ranking_metric_k

    ranking_results = db.session.query(Result).join(Result.experiment).filter_by(experiment_setting=self).filter(Result.metric_id == metric.id, Result.metric_k == metric_k).all()
    if len(ranking_results) == 0:
      return None

    if metric.minimise:
      best_function = min
    else:
      best_function = max
    best_result = best_function(ranking_results, key=lambda item:item.value)

    return best_result.experiment

  def get_algorithm_ranking(self, ranking_metric=None, ranking_metric_k=None):
    from app.models.experiments import Experiment
    """
    Given an experiment_setting, determining the ranking of all algorithms in that experiment_setting
    """

    metric = self.ranking_metric if ranking_metric is None else ranking_metric
    metric_k = self.ranking_metric_k if ranking_metric_k is None else ranking_metric_k

    if metric.minimise:
      best_function = lambda x:func.min(x).asc()
    else:
      best_function = lambda x:func.max(x).desc()

    query = db.session.query(Experiment.algorithm_id.label("algorithm_id"), func.rank().over(order_by=best_function(Result.value)).label("rank")) \
      .join(Result.experiment) \
      .join(Experiment.experiment_setting) \
      .filter(Experiment.experiment_setting == self) \
      .filter(Result.metric == metric, Result.metric_k == metric_k) \
      .group_by(Experiment.algorithm_id)

    return query.all()


class ExperimentSettingDisplaySchema(Schema):
  id = fields.Int()
  name = fields.Str()
  dataset = fields.Nested(DatasetSchema(only=("id", "name", "stats")))
  preprocessing = fields.Nested(ExperimentSettingFilterSchema(many=True))
  preprocessing_stats = fields.Nested(StatSchema)
  scenario = fields.Nested(ScenarioSchema(only=("id", "name")))
  scenario_params = fields.Dict()
  postprocessing = fields.Nested(ExperimentSettingFilterSchema(many=True))
  ranking_metric_id = fields.Str()
  ranking_metric_k = fields.Int()

  @pre_dump
  def from_database_record(self, record, **kwargs):
    return {
      "id": record.id,
      "name": record.name,
      "dataset": record.dataset,
      "preprocessing": [f for f in record.filters if f.is_preprocessing],
      "preprocessing_stats": {
        "interactions": record.interaction_count,
        "users": record.user_count,
        "items": record.item_count
      },
      "scenario": record.scenario,
      "scenario_params": record.scenario_params,
      "postprocessing": [f for f in record.filters if not f.is_preprocessing],
      "ranking_metric_id": record.ranking_metric_id,
      "ranking_metric_k": record.ranking_metric_k
    }

  class Meta:
    ordered = True
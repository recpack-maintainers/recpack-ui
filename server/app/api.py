from flask import Blueprint
from flask_restful import Resource, Api

from app.models import *

api_bp = Blueprint("api", __name__, url_prefix="/api")
api = Api(api_bp)

from app.resources.scenarios import ScenariosResource
api.add_resource(ScenariosResource, '/scenarios')

from app.resources.datasets import DatasetsResource
api.add_resource(DatasetsResource, '/datasets')

from app.resources.metrics import MetricsResource
api.add_resource(MetricsResource, '/metrics')

from app.resources.experiment_settings import ExperimentSettingsResource, ExperimentSettingResource, ExperimentSettingTemplateResource
api.add_resource(ExperimentSettingsResource, '/experiment-settings')
api.add_resource(ExperimentSettingResource, '/experiment-settings/<experiment_setting_id>')
api.add_resource(ExperimentSettingTemplateResource, '/experiment-settings/<experiment_setting_id>/template')

from app.resources.experiments import ExperimentResource
api.add_resource(ExperimentResource, '/experiments/<experiment_id>')

from app.resources.algorithms import AlgorithmsResource, AlgorithmResource
api.add_resource(AlgorithmsResource, '/algorithms')
api.add_resource(AlgorithmResource, '/algorithms/<algorithm_id>')

@api_bp.route("/")
def index():
    return "recex api"

import click
import json
import pandas as pd
from flask.cli import with_appcontext

from app import db
from app import utils
from app.models import *

@click.command("init-db")
@with_appcontext
def init_db_command():
    db.drop_all()
    db.create_all()



@click.command("seed-db")
@with_appcontext
def seed_db_command():
    utils.load_entities('seeds/datasets.json', DatasetSchema())
    utils.load_entities('seeds/scenarios.json', ScenarioSchema())
    utils.load_entities('seeds/filters.json', FilterSchema())
    utils.load_entities('seeds/metrics.json', MetricSchema())
    utils.load_entities('seeds/algorithms.json', AlgorithmSchema())
    db.session.commit()

@click.command("load-experiment")
@click.option('-m', '--many', is_flag=True)
@click.argument('file_path')
@with_appcontext
def load_experiment_command(file_path, many):
    with open(file_path, 'r') as f:
        experiments = utils.ExperimentLoader(many=many).load(json.load(f))
        print(experiments)

@click.command("load-setting")
@click.option('-m', '--many', is_flag=True)
@click.argument('file_path')
@with_appcontext
def load_setting_command(file_path, many):
    with open(file_path, 'r') as f:
        settings = utils.ExperimentSettingLoader(many=many).load(json.load(f))
        print(settings)

    
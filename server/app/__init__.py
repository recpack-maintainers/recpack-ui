import os

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
secret = ''

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    CORS(app, resources={r"/api/*": {"origins": "*"}})

    # default config
    app.config.from_mapping(
        DEBUG=os.environ.get("DEBUG", default=True),
        SECRET_KEY=os.environ.get("SECRET_KEY", default='secret'),
        SQLALCHEMY_DATABASE_URI=os.environ.get("DATABASE_URI", default=f"sqlite:///{app.instance_path}/database.db"),
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
    )

    # load the test config if passed in
    if test_config:
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    
    # Ensure FOREIGN KEY for sqlite3
    if 'sqlite' in app.config['SQLALCHEMY_DATABASE_URI']:
        def _fk_pragma_on_connect(dbapi_con, con_record):  # noqa
            dbapi_con.execute('pragma foreign_keys=ON')

        with app.app_context():
            from sqlalchemy import event
            event.listen(db.engine, 'connect', _fk_pragma_on_connect)

    secret = app.config['SECRET_KEY']

    # register flask commands
    from app.commands import init_db_command, seed_db_command, load_experiment_command, load_setting_command
    app.cli.add_command(init_db_command)
    app.cli.add_command(seed_db_command)
    app.cli.add_command(load_experiment_command)
    app.cli.add_command(load_setting_command)
    
    # register api endpoints
    from app import api
    app.register_blueprint(api.api_bp)

    return app
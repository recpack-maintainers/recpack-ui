import json
import pandas as pd
from flask import current_app
from marshmallow import ValidationError, Schema, fields, post_load
from sqlalchemy import func

from app import db
from app.models import *

def load_entities(file_path, schema):
  with open(file_path, 'r') as f:
    entity_list = json.load(f)

    try:
      entities = schema.load(entity_list, many=True)
      db.session.bulk_save_objects(entities)
    except ValidationError as err:
      print(err.messages)


class ExperimentLoader(Schema):
  algorithm_id = fields.Str()
  experiment_setting_id = fields.Int()
  hyperparameters = fields.Dict(load_default={})
  optimization_config = fields.Dict(load_default={})
  environment = fields.Dict(load_default={})
  model_size = fields.Int(load_default=-1)
  training_duration = fields.Int(load_default=-1)
  evaluation_duration = fields.Int(load_default=-1)
  results = fields.List(fields.Nested(ResultSchema()), load_default=[])

  @post_load
  def commit_to_database(self, data, **kwargs):
    e = Experiment(**{k:v for k,v in data.items() if k not in ['results']})
    db.session.add(e)
    db.session.commit()

    for result in data['results']:
      result.experiment_id = e.id
      db.session.add(result)
    db.session.commit()

    return e

class ExperimentSettingLoader(Schema):
  d = fields.Int()
  name = fields.Str()
  dataset_id = fields.Str()
  preprocessing_stats = fields.Nested(StatSchema, load_default=StatSchema().load({}))
  scenario_id = fields.Str()
  scenario_params = fields.Dict(load_default={})
  ranking_metric_id = fields.Str()
  ranking_metric_k = fields.Int(load_default=10)
  preprocessing = fields.Nested(ExperimentSettingFilterImportSchema(many=True), load_default=[])
  postprocessing = fields.Nested(ExperimentSettingFilterImportSchema(many=True), load_default=[])

  @post_load
  def commit_to_database(self, data, **kwargs):

    # Add setting itself to database
    es = ExperimentSetting(
      **{k: v for (k, v) in data.items() if k not in ['preprocessing_stats', 'preprocessing', 'postprocessing']},
      interaction_count=data["preprocessing_stats"]["interactions"],
      user_count=data["preprocessing_stats"]["users"],
      item_count=data["preprocessing_stats"]["items"],
    )
    db.session.add(es)
    db.session.commit()

    # Link filters to setting and add them to database
    for index, f in enumerate(data["preprocessing"]):
      f.order = index
      f.experiment_setting_id = es.id
      f.is_preprocessing = True
      db.session.add(f)

    for index, f in enumerate(data["postprocessing"]):
      f.order = index
      f.experiment_setting_id = es.id
      f.is_preprocessing = False
      db.session.add(f)

    db.session.commit()
    return es


def create_template(experiment_setting):
  def params_dict_to_string(params):
    result = ""
    for key, value in params.items():
      if isinstance(value, str):
        result += f"{key}=\"{value}\", "
      else:
        result += f"{key}={value}, "
    return result[:-2]

  def filters_import(filters):
    import_list = ''
    for f in filters:
      import_list += f'{f.filter.name}, '
    return import_list[:-2]
  
  def filters_steps(filters):
    result = ''
    for f in filters:
      result += f'dataset.add_filter({f.filter.name}({params_dict_to_string(f.params)}))\n'
    return result[:-1]


  # Read in the file
  with open('templates/template.py', 'r') as file :
    filedata = file.read()

  import_string = ''

  dataset = experiment_setting.dataset
  create_dataset = f'dataset = {dataset.class_name}("data", filename="{dataset.id}.csv", preprocess_default=False)'
  import_string += f'from recpack.datasets import {dataset.class_name}'

  scenario_class = experiment_setting.scenario.class_name
  scenario_params = params_dict_to_string(experiment_setting.scenario_params)
  create_scenario = f'scenario = {scenario_class}({scenario_params})'
  import_string += f'\nfrom recpack.scenarios import {scenario_class}'

  filters = experiment_setting.filters

  pre = [f for f in filters if f.is_preprocessing]
  pre_steps = ''
  if pre:
    import_string += f'\nfrom recpack.preprocessing.filters import {filters_import(pre)}' 
    pre_steps = filters_steps(pre)

  post = [f for f in filters if not f.is_preprocessing]
  post_steps = ''
  if post:
    import_string += f'\nfrom recpack.postprocessing.filters import {filters_import(post)}' 
    post_steps = filters_steps(post)

  experiment_setting_dict = { 
    "dataset_id": experiment_setting.dataset_id, 
    "preprocessing": ExperimentSettingFilterSchema2(many=True).dump([f for f in experiment_setting.filters if f.is_preprocessing]),
    "scenario_id": experiment_setting.scenario_id,
    "scenario_params": experiment_setting.scenario_params,
    "postprocessing": ExperimentSettingFilterSchema2(many=True).dump([f for f in experiment_setting.filters if not f.is_preprocessing]),
  }


  filedata = filedata.replace('IMPORTS', import_string)
  filedata = filedata.replace('CREATE_DATASET', create_dataset)
  filedata = filedata.replace('PREPROCESSING_STEPS', pre_steps)
  filedata = filedata.replace('CREATE_SCENARIO', create_scenario)
  filedata = filedata.replace('POSTPROCESSING_STEPS', post_steps)
  filedata = filedata.replace('EXPERIMENT_SETTING_DICT', f'experiment_setting = {experiment_setting_dict}')
 
  return filedata
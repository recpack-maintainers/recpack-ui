import pytest
from dotenv import load_dotenv
import tempfile
import os
import io


from app import create_app, db, utils
from app.models import *

load_dotenv()

def generate_entities(schema, name='entity', count=1):
  for i in range(0, count):
    db.session.add(schema.load({
      "id": f"{name}{i+1}",
      "name": f"{name.capitalize()} {i+1}",
      "description": "",
      "recpack": "http://example.com",
    }))
  db.session.commit()


def generate_static_data(count=1):
  generate_entities(DatasetSchema(), 'dataset', count)
  generate_entities(ScenarioSchema(), 'scenario', count)
  generate_entities(FilterSchema(), 'filter', count)
  generate_entities(MetricSchema(), 'metric', count)
  generate_entities(AlgorithmSchema(), 'algorithm', count)


def create_experiment_setting(ranking_metric_id, ranking_metric_k):
    es = utils.ExperimentSettingLoader().load({
      'name': 'Experiment Setting',
      'dataset_id': 'dataset1',
      'scenario_id': 'scenario1',
      'ranking_metric_id': ranking_metric_id,
      'ranking_metric_k': ranking_metric_k
    })

    return es

 
def create_experiment(experiment_setting, algorithm_id, results):
  prepared_results = []
  for result in results:
    if type(result) == float:
      prepared_results.append({
        "metric_id": experiment_setting.ranking_metric_id,
        "metric_k": experiment_setting.ranking_metric_k,
        "value": result
      })
    else:
       prepared_results.append({
        "metric_id": result['metric_id'],
        "metric_k": result['metric_k'],
        "value": result['value']
      })
  
  e = utils.ExperimentLoader().load({
    "algorithm_id": algorithm_id,
    "experiment_setting_id": experiment_setting.id,
    "results": prepared_results
  })

  return e


@pytest.fixture
def app():
    db_fd, db_fname = tempfile.mkstemp()

    config = {
      "SQLALCHEMY_DATABASE_URI": os.environ.get("TEST_DATABASE_URI", default="postgresql:///test?user=app"),
      "TESTING": True
    }

    app = create_app(config)

    with app.app_context():
      db.reflect()
      db.drop_all()
      db.create_all()
      
      yield app

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


class TestExperimentSettingTopExperiment:
  """
  test ExperimentSetting.get_top_experiment
  """
  
  def test_happy_day(self, app):
    """
    top experiment out of:
      experiment1 with result = 0.1
      experiment2 with result = 0.2
      experiment3 with result = 0.3
    should be experiment3
    """
    generate_static_data(1)
    es = create_experiment_setting('metric1', 10)
    e1 = create_experiment(es, 'algorithm1', [0.1])
    e2 = create_experiment(es, 'algorithm1', [0.2])
    e3 = create_experiment(es, 'algorithm1', [0.3])
    
    assert es.get_top_experiment() == e3

  def test_minimising_metric(self, app):
    """
    top experiment out of:
      experiment1 with result = 0.1
      experiment2 with result = 0.2
      experiment3 with result = 0.3
    for experiment setting with minimising ranking metric
    should be experiment1
    """
    generate_static_data(1)
    es = create_experiment_setting('metric1', 10)
    es.ranking_metric.minimise = True
    db.session.commit()

    e1 = create_experiment(es, 'algorithm1', [0.1])
    e2 = create_experiment(es, 'algorithm1', [0.2])
    e3 = create_experiment(es, 'algorithm1', [0.3])
    
    assert es.get_top_experiment() == e1

  def test_multiple_results(self, app):
    """
    top experiment out of:
      experiment1 with metric1@10 = 0.1, metric1@20 = 0.3, metric2@10 = 0.3, metric2@20 = 0.3
      experiment2 with metric1@10 = 0.2, metric1@20 = 0.1, metric2@10 = 0.1, metric2@20 = 0.1
      experiment3 with metric2@20 = 0.9
      experiment4 with no results
    for experiment setting with default ranking metric = metric1@10
    should be experiment2
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    e1 = create_experiment(es, 'algorithm1', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.3
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.3
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.3
      },
    ])
    e2 = create_experiment(es, 'algorithm1', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.2
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.1
      },
    ])
    e3 = create_experiment(es, 'algorithm1', [
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.9
      },
    ])
    e4 = create_experiment(es, 'algorithm1', [])
    
    assert es.get_top_experiment() == e2
  
  def test_multiple_experiment_settings(self, app):
    """
    top experiment out of:
      experiment1 with result = 0.1
      experiment2 with result = 0.2
      experiment3 with result = 0.9 for different experiment setting
    should be experiment2
    """
    generate_static_data(2)
    es1 = create_experiment_setting('metric1', 10)
    es2 = create_experiment_setting('metric1', 10)
    e1 = create_experiment(es1, 'algorithm1', [0.1])
    e2 = create_experiment(es1, 'algorithm1', [0.2])
    e3 = create_experiment(es2, 'algorithm1', [0.9])
    
    assert es1.get_top_experiment() == e2

  def test_override_ranking_metric(self, app):
    """
    top experiment out of:
      experiment1 with metric1@10 = 0.4
      experiment2 with metric1@20 = 0.3
      experiment3 with metric2@10 = 0.2
      experiment4 with metric2@20 = 0.1
    where ranking metric is overriden as metric2@20
    should be experiment4
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    e1 = create_experiment(es, 'algorithm1', [{
      'metric_id': 'metric1',
      'metric_k': 10,
      'value': 0.4
    }])
    e2 = create_experiment(es, 'algorithm1', [{
      'metric_id': 'metric1',
      'metric_k': 20,
      'value': 0.3
    }])
    e3 = create_experiment(es, 'algorithm1', [{
      'metric_id': 'metric2',
      'metric_k': 10,
      'value': 0.2
    }])
    e4 = create_experiment(es, 'algorithm1', [{
      'metric_id': 'metric2',
      'metric_k': 20,
      'value': 0.1
    }])
    
    assert es.get_top_experiment(ranking_metric=db.session.query(Metric).get('metric2'), ranking_metric_k=20) == e4


class TestExperimentSettingAlgorithmRanking:
  """
  test ExperimentSetting.get_algorithm_ranking
  """

  @staticmethod
  def check_ranking(ranking, expected_ranking):
    assert len(ranking) == len(expected_ranking)

    for i, (r, er) in enumerate(zip(ranking, expected_ranking)):
      assert r.algorithm_id == er
      assert r.rank == i+1

  def test_happy_day(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with ranking metric = 0.1
      experiment2(algorithm2) with ranking metric = 0.2
    should be
      1. algorithm2
      2. algorithm1
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    
    create_experiment(es, 'algorithm1', [0.1])
    create_experiment(es, 'algorithm2', [0.2])

    ranking = es.get_algorithm_ranking()
    self.check_ranking(ranking, ['algorithm2', 'algorithm1'])

  def test_minimising_metric(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with ranking metric = 0.1
      experiment2(algorithm2) with ranking metric = 0.2
    for experiment with minimising ranking metric
    should be
      1. algorithm1
      2. algorithm2
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    es.ranking_metric.minimise = True
    db.session.commit()
    
    create_experiment(es, 'algorithm1', [0.1])
    create_experiment(es, 'algorithm2', [0.2])

    ranking = es.get_algorithm_ranking()
    self.check_ranking(ranking, ['algorithm1', 'algorithm2'])

  def test_multiple_experiments(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with ranking metric = 0.1
      experiment2(algorithm2) with ranking metric = 0.2
      experiment4(algorithm2) with ranking metric = 0.3
      experiment3(algorithm1) with ranking metric = 0.4
    should be
      1. algorithm1
      2. algorithm2
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    
    create_experiment(es, 'algorithm1', [0.1])
    create_experiment(es, 'algorithm2', [0.2])
    create_experiment(es, 'algorithm2', [0.3])
    create_experiment(es, 'algorithm1', [0.4])

    ranking = es.get_algorithm_ranking()
    self.check_ranking(ranking, ['algorithm1', 'algorithm2'])

  def test_multiple_results(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with metric1@10 = 0.1, metric1@20 = 0.3, metric2@10 = 0.3, metric2@20 = 0.3
      experiment2(algorithm2) with metric1@10 = 0.2, metric1@20 = 0.1, metric2@10 = 0.1, metric2@20 = 0.1
      experiment3(algorithm3) with metric2@20 = 0.9
      experiment4(algorithm4) with no results
    should be
      1. algorithm2
      2. algorithm1
      no rank. algortihm3, algorithm4
    """
    generate_static_data(4)
    es = create_experiment_setting('metric1', 10)
    
    create_experiment(es, 'algorithm1', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.3
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.3
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.3
      },
    ])
    create_experiment(es, 'algorithm2', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.2
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.1
      },
    ])
    create_experiment(es, 'algorithm3', [
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.9
      },
    ])
    create_experiment(es, 'algorithm4', [])

    ranking = es.get_algorithm_ranking()
    self.check_ranking(ranking, ['algorithm2', 'algorithm1'])

  def test_multiple_experiment_settings(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with result = 0.1
      experiment2(algorithm2) with result = 0.2
      experiment3(algorithm1) with result = 0.9 for different experiment setting
    should be:
      1. algorithm2
      2. algorithm1
    """
    generate_static_data(2)
    es1 = create_experiment_setting('metric1', 10)
    es2 = create_experiment_setting('metric1', 10)
    
    create_experiment(es1, 'algorithm1', [0.1])
    create_experiment(es1, 'algorithm2', [0.2])
    create_experiment(es2, 'algorithm1', [0.9])
    
    ranking = es1.get_algorithm_ranking()
    self.check_ranking(ranking, ['algorithm2', 'algorithm1'])

  def test_override_ranking_metric(self, app):
    """
    algorithm ranking for
      experiment1(algorithm1) with metric1@10 = 0.1, metric1@20 = 0.2, metric2@10 = 0.2, metric2@20 = 0.2
      experiment2(algorithm2) with metric1@10 = 0.3, metric1@20 = 0.1, metric2@10 = 0.1, metric2@20 = 0.1
    where ranking metric is overriden as metric2@20
    should be:
      1. algorithm1
      2. algorithm2
    """
    generate_static_data(2)
    es = create_experiment_setting('metric1', 10)
    
    create_experiment(es, 'algorithm1', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.2
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.2
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.2
      },
    ])
    create_experiment(es, 'algorithm2', [
      {
        'metric_id': 'metric1',
        'metric_k': 10,
        'value': 0.3
      },
      {
        'metric_id': 'metric1',
        'metric_k': 20,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 10,
        'value': 0.1
      },
      {
        'metric_id': 'metric2',
        'metric_k': 20,
        'value': 0.1
      },
    ])

    ranking = es.get_algorithm_ranking(ranking_metric=db.session.query(Metric).get('metric2'), ranking_metric_k=20)
    self.check_ranking(ranking, ['algorithm1', 'algorithm2'])


class TestAlgorithmRankings:
  """
  test Algorithm.get_rankings
  """
  
  def test_happy_day(self, app):
    generate_static_data(2)
    es1 = create_experiment_setting('metric1', 10)
    es2 = create_experiment_setting('metric1', 10)
    es3 = create_experiment_setting('metric1', 10)
    algorithm1 = db.session.query(Algorithm).get('algorithm1')
    algorithm2 = db.session.query(Algorithm).get('algorithm2')
    
    create_experiment(es1, algorithm1.id, [0.1])
    create_experiment(es1, algorithm2.id, [0.2])
    create_experiment(es2, algorithm1.id, [0.2])
    create_experiment(es2, algorithm2.id, [0.1])
    create_experiment(es3, algorithm1.id, [0.1])
    create_experiment(es3, algorithm2.id, [0.2])

    ranking1 = algorithm1.get_rankings()
    ranking2 = algorithm2.get_rankings()
    assert ranking1 == {
      es1.id: 2,
      es2.id: 1,
      es3.id: 2,
    }
    assert ranking2 == {
      es1.id: 1,
      es2.id: 2,
      es3.id: 1,
    }

  def test_minimising_metroc(self, app):
    generate_static_data(2)
    es1 = create_experiment_setting('metric1', 10)
    es2 = create_experiment_setting('metric2', 10)
    es2.ranking_metric.minimise = True
    db.session.commit()
    algorithm1 = db.session.query(Algorithm).get('algorithm1')
    algorithm2 = db.session.query(Algorithm).get('algorithm2')
    
    create_experiment(es1, algorithm1.id, [0.1])
    create_experiment(es1, algorithm2.id, [0.2])
    create_experiment(es2, algorithm1.id, [0.2])
    create_experiment(es2, algorithm2.id, [0.1])

    ranking1 = algorithm1.get_rankings()
    ranking2 = algorithm2.get_rankings()
    assert ranking1 == {
      es1.id: 2,
      es2.id: 2,
    }
    assert ranking2 == {
      es1.id: 1,
      es2.id: 1,
    }

  def test_multiple_experiment_settings(self, app):
    generate_static_data(2)
    es1 = create_experiment_setting('metric1', 10)
    es2 = create_experiment_setting('metric1', 10)
    es3 = create_experiment_setting('metric1', 10)

    algorithm1 = db.session.query(Algorithm).get('algorithm1')
    algorithm2 = db.session.query(Algorithm).get('algorithm2')
    
    create_experiment(es1, algorithm1.id, [0.1])
    create_experiment(es1, algorithm2.id, [0.2])
    create_experiment(es2, algorithm1.id, [0.1])
    create_experiment(es3, algorithm2.id, [0.1])

    ranking1 = algorithm1.get_rankings()
    ranking2 = algorithm2.get_rankings()
    assert ranking1 == {
      es1.id: 2,
      es2.id: 1,
    }
    assert ranking2 == {
      es1.id: 1,
      es3.id: 1,
    }


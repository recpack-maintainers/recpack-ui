from setuptools import find_packages, setup

setup(
  name='recex',
  version='0.0.1',
  packages=find_packages(),
  include_package_data=True,
  zip_safe=False,
  install_requires=[
    'flask==2.0.3',
    'flask-restful==0.3.9',
    'flask-cors==3.0.10',
    'Flask-SQLAlchemy==2.5.1',
    'pandas',
    'marshmallow',
    'gunicorn',
    'python-dotenv'
  ]
)

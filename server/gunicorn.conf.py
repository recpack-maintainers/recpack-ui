bind = 'unix:recex.sock'
workers = 3
umask = 7
accesslog = 'logs/access.log'
access_log_format = '%(t)s "%(r)s" %(s)s'
errorlog = 'logs/error.log'
capture_output = True

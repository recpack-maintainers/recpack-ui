# RecEx server

## Instalation

```sh
    # create and activate virtual python environment
    python3 -m venv env
    source venv/bin/activate

    # install server
    pip3 install -e .

    # initialize and populate database
    flask init-db # creates the database
    flask seed-db # seeds the database with static data such as the different datasets, scenarios, filters, etc. available in recpack
    flask load-setting -m seeds/experiment_settings.json # loads sample experiment settings
    flask load-experiment -m seeds/experiments/test.json # loads sample experiment results

    # run server
    flask run
```

## Endpoints

Every endpoint has a class which contains it's logic. These can be found under `app/resources` and are linked to urls by `app/api.py`.

### Static data

For requesting information about entities in recpack the following endpoints are used:

- `GET api/algorithms` returns the available algorithms
- `GET api/datasets` returns the available datasets
- `GET api/metrics` returns the available metrics
- `GET api/scenarios` returns the available scenarios

### Experiment Settings

Experiment settings are curated setups for running experiments. They specify the dataset, processing filters, scenario parameters, etc.

- `GET api/experiment-settings` returns the available experiment settings
- `GET api/experiment-settings/<id>` returns a specific experiment settings + all experiment results for that setting
- `GET api/experiment-settings/<id>/template` downloads a python template for running an experiment in the setting

### Experiment Results

An experiment result contains information about the execution of a single algorithm within a specific experiment setting. Reproduction steps (such as environment, optimization configuration and final hyperparameters) are saved as well as results for one or more metrics.

- `GET api/experiments/<id>` returns a specific experiment result
- (see also `GET api/experiment-settings/<id>` which returns all results for a given setting)

## Adding/Updating data

### Static data

Currently the only way to change the static data is the manually change it in the database.

### Experiment settings and results

To add new experiment settings use the command `flask load-setting <file>.json`. If multiple settings are described in the file, add the `-m` flag.

Adding experiment results works similar with the `flask load-experiment [-m] <file>.json` command.

### TODO

- Allow for easy/automatic updating of the "static data"
  - could be based on recpack updates
  - how do we preserve concistency with existing experiments and experiment settings?
    - experiment settings and experiments have foreign keys referencing datasets, scenarios, filters, metrics and algorithms
    - maybe mostly focus on adding new entities and disallow drastically changing/deleting existing entities (because previous experiments will become uncomparable/will be deleted by cascade)
  - maybe an easy to use admin control panel?
- Adding and removing experiment settings could also use an admin control panel
  - allow removing settings? maybe warn if there are many results because they will be deleted
  - adding settings by selecting from available datasets/scenarios/filters.
- Submision of experiment results and new algorithms
  - unreviewd submissions could be collected in control panel for manual review
  - maybe assisted by automatic filtering
    - based on user (number of submissions already accepted)
    - the metric results (if they are wildly different from other results in the setting)

export type TStats = {
  interactions: number
  items: number
  users: number
}

export type TFilter = {
  filter: {
    id: string
    name: string
    description: string
  }
  params: Record<string, any>
}

export type TExperimentSetting = {
  id: number
  name: string
  dataset: {
    stats: TStats
    id: string
    name: string
  }
  preprocessing: TFilter[]
  preprocessing_stats: TStats
  scenario: {
    id: string
    name: string
  }
  scenario_params: Record<string, any>
  postprocessing: TFilter[]
  ranking_metric_id: string
  ranking_metric_k: number
  top_experiment: {
    algorithm_id: string
    algorithm_name: string
    experiment_id: number
  } | null
  experiment_count: number
}

export type TAlgorithm = {
  id: string
  name: string
}

export type TExperiment = {
  id: number
  algorithm: TAlgorithm
  experiment_setting: {
    id: number
    name: string
    dataset: {
      id: number
      name: string
    }
    scenario: {
      id: number
      name: string
    }
  }
  hyperparameters: Record<string, any>
  optimization_config: Record<string, any>
  results: Array<{
    metric_id: string
    metric_k: number
    value: number
  }>
  model_size: number
  training_duration: number
  evaluation_duration: number
  environment: Record<string, any> & { dep: Record<string, any> }
  verified: boolean
  uploader?: {
    username: string
  }
  created_timestamp: string
}

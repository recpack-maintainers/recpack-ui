export default [
  { path: "/", component: () => import("../pages/Home.vue") },

  {
    path: "/datasets",
    component: () => import("../pages/Datasets.vue"),
    props: (route: any) => ({ focus: route.query.focus }),
  },
  {
    path: "/scenarios",
    component: () => import("../pages/Scenarios.vue"),
    props: (route: any) => ({ focus: route.query.focus }),
  },
  {
    path: "/experiment-settings",
    component: () => import("../pages/ExperimentSettings.vue"),
    props: (route: any) => ({ scenarioQuery: route.query.scenario, datasetQuery: route.query.dataset }),
  },
  {
    path: "/experiment-settings/:id",
    component: () => import("../pages/ExperimentSetting.vue"),
    props: (route: any) => ({ focus: route.query.focus }),
  },
  {
    path: "/algorithms",
    component: () => import("../pages/Algorithms.vue"),
    props: (route: any) => ({ focus: route.query.focus }),
  },
  {
    path: "/algorithms/:id",
    component: () => import("../pages/Algorithm.vue"),
    props: (route: any) => ({ focus: route.query.focus }),
  },
]

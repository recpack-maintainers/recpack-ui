export function formattedSize(size: number, binary: boolean): string {
  if (size === 0) return "0 bytes"
  if (size === 1) return "1 byte"
  const power = binary ? 1024 : 1000
  const symbols = binary ? ["bytes", "kiB", "MiB", "GiB", "TiB"] : ["bytes", "kB", "MB", "GB", "TB"]

  let i = Math.floor(Math.log(size) / Math.log(power))
  return Number((size / Math.pow(power, i)).toFixed(2)) + " " + symbols[i]
}

export function formatDuration(duration: number): string {
  let date = new Date(1000 * duration)
  return date.toISOString().substring(11, 19)
}

const numberFormatter = Intl.NumberFormat("en", { notation: "compact" })
export function formatNumber(n: number): string {
  return numberFormatter.format(n)
}

import { defineStore } from "pinia"

const fetcher = async (name: string) => {
  const raw = await fetch(`${import.meta.env.VITE_API_URL}/${name}`)
  return await raw.json()
}

export type TDataset = {
  id: string
  name: string
  description: string
  recpack: string
  class_name: string
  stats: {
    items: number
    users: number
    interactions: number
  }
}

export type TScenario = {
  id: string
  name: string
  description: string
  recpack: string
  class_name: string
}

export type TAlgorithm = {
  id: string
  name: string
  description: string
  experiment_count: number
  experiment_settings_count: number
}

export type TMetric = {
  id: string
  name: string
  minimise: boolean
}

export const useRecpackStore = defineStore("recpack", {
  state: () => ({
    datasets: [] as TDataset[],
    scenarios: [] as TScenario[],
    algorithms: [] as TAlgorithm[],
    metrics: {} as Record<string, TMetric>,
  }),
  getters: {},
  actions: {
    async loadScenarios() {
      this.scenarios = await fetcher("scenarios")
    },
    async loadDatasets() {
      this.datasets = await fetcher("datasets")
    },
    async loadAlgorithms() {
      this.algorithms = await fetcher("algorithms")
    },
    async loadMetrics() {
      const metricsList = await fetcher("metrics")
      this.metrics = Object.fromEntries(metricsList.map((el: TMetric) => [el.id, el]))
    },
    async loadAll() {
      this.loadScenarios()
      this.loadDatasets()
      this.loadAlgorithms()
      this.loadMetrics()
    },
    getMetricDisplayName(metric_id: string, metric_k: number) {
      if (metric_id in this.metrics) return `${this.metrics[metric_id].name}@${metric_k}`
      else return "unknown"
    },
  },
})

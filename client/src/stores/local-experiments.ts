import { defineStore } from "pinia"
import Vue from "vue"

import Ajv, { JSONSchemaType } from "ajv"
import { TExperimentSetting, TFilter } from "../format"
const ajv = new Ajv()

type TLocalFilter = {
  filter_id: string
  params: Record<string, any>
}
type TLocalExperimentSetting = {
  dataset_id: string
  scenario_id: string
  scenario_params: Record<string, any>
  preprocessing: TLocalFilter[]
  postprocessing: TLocalFilter[]
}
export type TLocalExperiment = {
  experiment_setting: TLocalExperimentSetting
  algorithm_name: string
  hyperparameters: Record<string, any>
  results: Array<{
    metric_id: string
    metric_k: number
    value: number
  }>
  model_size: number
  optimization_duration: number
  training_duration: number
  evaluation_duration: number
  environment: Record<string, any> & { dep: Record<string, any> }
  optimization_config?: Record<string, any>
  created_timestamp: number
}

const fschema: JSONSchemaType<TLocalFilter> = {
  type: "object",
  required: ["filter_id", "params"],
  additionalProperties: false,
  properties: {
    filter_id: { type: "string" },
    params: { type: "object" },
  },
}
const schema: JSONSchemaType<TLocalExperiment> = {
  type: "object",
  required: [
    "experiment_setting",
    "algorithm_name",
    "hyperparameters",
    "results",
    "model_size",
    "training_duration",
    "evaluation_duration",
    "environment",
    "created_timestamp",
    "optimization_duration",
  ],
  additionalProperties: false,
  properties: {
    experiment_setting: {
      type: "object",
      required: ["dataset_id", "scenario_id", "scenario_params", "preprocessing", "postprocessing"],
      additionalProperties: false,
      properties: {
        dataset_id: { type: "string" },
        scenario_id: { type: "string" },
        scenario_params: {
          type: "object",
        },
        preprocessing: {
          type: "array",
          items: fschema,
        },
        postprocessing: {
          type: "array",
          items: fschema,
        },
      },
    },
    algorithm_name: { type: "string" },
    hyperparameters: { type: "object" },
    results: {
      type: "array",
      items: {
        type: "object",
        required: ["metric_id", "metric_k", "value"],
        additionalProperties: false,
        properties: {
          metric_id: { type: "string" },
          metric_k: { type: "number" },
          value: { type: "number" },
        },
      },
    },
    model_size: { type: "number" },
    optimization_duration: { type: "number" },
    training_duration: { type: "number" },
    evaluation_duration: { type: "number" },
    created_timestamp: { type: "number" },
    environment: { type: "object" },
    optimization_config: { type: "object", nullable: true },
  },
}
const validate = ajv.compile(schema)

function verifySetting(localSetting: TLocalExperimentSetting, experimentSetting: TExperimentSetting) {
  function verifyParams(localParams: Record<string, any>, actualParams: Record<string, any>): boolean {
    // check if local parameters dict has correct amount of entries
    if (Object.keys(localParams).length !== Object.keys(actualParams).length) return false

    // check if all local parameters match
    return Object.entries(localParams).every((pair) => {
      const k = pair[0]
      const v = pair[1]
      if (actualParams[k] !== v) return false
      return true
    })
  }
  function verifyFilters(localFilters: TLocalFilter[], actualFilters: TFilter[]) {
    // check if local filters array is an array of correct length
    if (localFilters.length !== actualFilters.length) return false

    // check if all local filters match
    return actualFilters.every((actualFilter, index) => {
      const localFilter = localFilters[index]
      if (localFilter.filter_id !== actualFilter.filter.id) return false
      if (!verifyParams(localFilter.params, actualFilter.params)) return false
      return true
    })
  }

  if (localSetting.dataset_id !== experimentSetting.dataset.id) return false
  if (localSetting.scenario_id !== experimentSetting.scenario.id) return false
  if (!verifyParams(localSetting.scenario_params, experimentSetting.scenario_params)) return false
  if (!verifyFilters(localSetting.preprocessing, experimentSetting.preprocessing)) return false
  if (!verifyFilters(localSetting.postprocessing, experimentSetting.postprocessing)) return false
  return true
}

export function loadLocalExperiment(
  rawString: any,
  experimentSetting: TExperimentSetting
): { result: TLocalExperiment | null; errors: string[] } {
  try {
    const data = JSON.parse(rawString)

    if (validate(data)) {
      if (verifySetting(data.experiment_setting, experimentSetting)) {
        return {
          result: data,
          errors: [],
        }
      } else {
        return {
          result: null,
          errors: ["Experiment setting does not match."],
        }
      }
    } else {
      return {
        result: null,
        errors: validate.errors?.map((e) => e?.message || "") || [],
      }
    }
  } catch {
    return {
      result: null,
      errors: ["Invalid json."],
    }
  }
}

export const useLocalExperimentsStore = defineStore("local-experiments", {
  state: () => ({ experiments: {} as Record<string, TLocalExperiment[]> }),
  getters: {
    experimentsFlat(): TLocalExperiment[] {
      return Object.entries(this.experiments)
        .map((pair) => pair[1])
        .flat()
    },
  },
  actions: {
    addExperiment(e: TLocalExperiment) {
      if (!(e.algorithm_name in this.experiments)) Vue.set(this.experiments, e.algorithm_name, [])
      this.experiments[e.algorithm_name].push(e)
    },
    removeExperiment(algorithm_name: string, index: number) {
      const arr = this.experiments[algorithm_name]
      if (index < arr.length) arr.splice(index, 1)
    },
    clearExperiments() {
      this.experiments = {}
    },
    removeExperiments(algorithm_name: string) {
      Vue.set(this.experiments, algorithm_name, [])
    },
  },
})

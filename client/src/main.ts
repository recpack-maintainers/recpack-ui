import Vue from "vue"
import "./style.css"
import App from "./App.vue"

// Vue Select
import vSelect from "vue-select"
import "vue-select/dist/vue-select.css"
Vue.component("v-select", vSelect)
// @ts-ignore
vSelect.props.components.default = () => ({
  OpenIndicator: {
    render: (createElement: (arg0: string, arg1: Object) => any) =>
      createElement("font-awesome-icon", {
        props: {
          icon: "fa-solid fa-chevron-down",
        },
        class: "text-gray-600 px-1",
      }),
  },
})

// Font awesome
import { library } from "@fortawesome/fontawesome-svg-core"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
import {
  faChevronDown,
  faChevronUp,
  faArrowRight,
  faList,
  faFlask,
  faDatabase,
  faCircleRight,
  faFilter,
  faTrophy,
  faBook,
  faHouse,
  faCalculator,
  faScroll,
  faArrowUp,
  faArrowDown,
  faUser,
  faCertificate,
  faCircleExclamation,
  faCircleCheck,
  faCircleInfo,
  faCircleQuestion,
  faXmark,
  faTrash,
  faFileImport,
  faFileCirclePlus,
} from "@fortawesome/free-solid-svg-icons"
import { faCalendar } from "@fortawesome/free-regular-svg-icons"
library.add(
  faChevronDown,
  faChevronUp,
  faArrowRight,
  faList,
  faFlask,
  faDatabase,
  faCircleRight,
  faFilter,
  faTrophy,
  faBook,
  faHouse,
  faCalculator,
  faScroll,
  faArrowUp,
  faArrowDown,
  faCalendar,
  faUser,
  faCertificate,
  faCircleCheck,
  faCircleInfo,
  faCircleQuestion,
  faCircleExclamation,
  faXmark,
  faTrash,
  faFileImport,
  faFileCirclePlus
)
Vue.component("font-awesome-icon", FontAwesomeIcon)

// vue router
import VueRouter from "vue-router"
import routes from "./router/routes.js"
const router = new VueRouter({
  routes,
})
Vue.use(VueRouter)

// pinia (state management)
import { createPinia, PiniaVuePlugin } from "pinia"
Vue.use(PiniaVuePlugin)
const pinia = createPinia()

new Vue({
  router,
  pinia,
  render: (h) => h(App),
}).$mount("#app")

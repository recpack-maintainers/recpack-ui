| Model  | Recall@10 | NDCG@10 | DCG@15 |
| ------ | --------- | ------- | ------ |
| slim   | 0.6       | 0.3     | 0.1    |
| ease   | 0.5       | -       | 0.5    |
| itemcf | 0.2       | 0.6     | 0.2    |

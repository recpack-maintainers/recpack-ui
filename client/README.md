# RecEx client

## Instalation

```sh
    # install dependencies
    npm install

    # run in development
    npm run dev

    # build for production
    npm run build
```

## Project structure

- `src/main.ts entry point for vue app
- `src/App.vue` control the main layout with tabs and defines which tabs are shown
- `src/pages` contain the main pages accesible through the tabs
- `src/components` contain components used in the pages
- `src/stores` contain globaly accesible datastores that can be access from each page or component
  - `recpack.ts` when page load fetches static data for datasets, scenarios, metrics and algorithms. All components can use this information without needing to request it each time
  - `local-experiments.ts` stores local experiments. Is only used in the ExperimentSetting page, but eases the dataflow significantly. Could perhape later be used to persist it between page reloads
- `src/router/routes` links urls to pages
- `src/format.ts` contains typescript aliasses

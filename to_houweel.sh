#!/bin/bash
set -x # echo on



#=== Front end ===#
echo "Building and copying front-end"

# build
cd ./client
npm run build

# copy
scp -r ./dist/* houweel:/home/www/recex/client

cd .. 
#=== Front end ===#



#=== API ===#
echo "Building and installing API"

# build 
cd ./server
python3 -m build

# copy
scp ./dist/recex-0.0.1-py3-none-any.whl gunicorn.conf.py houweel:/home/www/recex/server
scp -r ./seeds/* houweel:/home/www/recex/server/seeds
scp -r ./templates/* houweel:/home/www/recex/server/templates

# install
echo "Installing without dependencies. If dependencies changed, run without --no-deps"
ssh houweel "cd /home/www/recex/server && . env/bin/activate && pip install recex-0.0.1-py3-none-any.whl --force-reinstall --no-deps"

cd ..
#=== API ===#



#=== Services ===#
echo "Copying service files"

# copy
scp -r ./services/*.service houweel:/home/www/recex/services/
scp -r ./appache/* houweel:/home/www/recex/appache/

# Remind to restart services
echo "Remember to restart the services to reflect changes in the application:"
echo "sudo systemctl restart recex"
echo ""
echo "Or if the services changed, first run:"
echo "sudo systemctl daemon-reload"

cd ..
#=== Services ===#